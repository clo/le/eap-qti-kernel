/* Copyright (c) 2018-2020, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 */

/ {
	aliases {
		mhi2 = &mhi_2;
		mhi_netdev5 = &mhi_netdev_5;
		mhi_netdev7 = &mhi_netdev_7;
	};

	ssr: qcom,pcie_ssr {
		compatible = "qcom,pcie_ssr";
	};

	mhi_2: qcom,mhi-sa515m {
		compatible = "qcom,mhi";

		qcom,pci-dev-id = <0x0306>;
		qcom,pci-domain = <0>;
		qcom,pci-bus = <1>;
		qcom,pci-slot = <0>;
		qcom,le-endpoint;

		/* controller specific configuration */
		qcom,smmu-cfg = <0x3>;

		/* mhi bus specific settings */
		mhi,max-channels = <110>;
		mhi,timeout = <2000>;
		mhi,buffer-len = <0x4000>;

		mhi_channels {
			#address-cells = <1>;
			#size-cells = <0>;

			mhi_chan@0 {
				reg = <0>;
				label = "LOOPBACK";
				mhi,num-elements = <64>;
				mhi,event-ring = <2>;
				mhi,chan-dir = <1>;
				mhi,data-type = <0>;
				mhi,doorbell-mode = <2>;
				mhi,ee = <0x4>;
			};

			mhi_chan@1 {
				reg = <1>;
				label = "LOOPBACK";
				mhi,num-elements = <64>;
				mhi,event-ring = <2>;
				mhi,chan-dir = <2>;
				mhi,data-type = <0>;
				mhi,doorbell-mode = <2>;
				mhi,ee = <0x4>;
			};

			mhi_chan@4 {
				reg = <4>;
				label = "DIAG";
				mhi,num-elements = <64>;
				mhi,event-ring = <1>;
				mhi,chan-dir = <1>;
				mhi,data-type = <0>;
				mhi,doorbell-mode = <2>;
				mhi,ee = <0x4>;
			};

			mhi_chan@5 {
				reg = <5>;
				label = "DIAG";
				mhi,num-elements = <64>;
				mhi,event-ring = <3>;
				mhi,chan-dir = <2>;
				mhi,data-type = <0>;
				mhi,doorbell-mode = <2>;
				mhi,ee = <0x4>;
			};

			mhi_chan@20 {
				reg = <20>;
				label = "IPCR";
				mhi,num-elements = <64>;
				mhi,event-ring = <1>;
				mhi,chan-dir = <1>;
				mhi,data-type = <1>;
				mhi,doorbell-mode = <2>;
				mhi,ee = <0x4>;
				mhi,auto-start;
			};

			mhi_chan@21 {
				reg = <21>;
				label = "IPCR";
				mhi,num-elements = <64>;
				mhi,event-ring = <2>;
				mhi,chan-dir = <2>;
				mhi,data-type = <0>;
				mhi,doorbell-mode = <2>;
				mhi,ee = <0x4>;
				mhi,auto-queue;
				mhi,auto-start;
			};

			mhi_chan@46 {
				reg = <46>;
				label = "IP_SW0";
				mhi,num-elements = <512>;
				mhi,event-ring = <4>;
				mhi,chan-dir = <1>;
				mhi,data-type = <1>;
				mhi,doorbell-mode = <2>;
				mhi,ee = <0x4>;
			};

			mhi_chan@47 {
				reg = <47>;
				label = "IP_SW0";
				mhi,num-elements = <512>;
				mhi,event-ring = <5>;
				mhi,chan-dir = <2>;
				mhi,data-type = <4>;
				mhi,doorbell-mode = <2>;
				mhi,ee = <0x4>;
			};

			mhi_chan@100 {
				reg = <100>;
				label = "IP_HW0";
				mhi,num-elements = <512>;
				mhi,event-ring = <6>;
				mhi,chan-dir = <1>;
				mhi,data-type = <1>;
				mhi,doorbell-mode = <3>;
				mhi,ee = <0x4>;
				mhi,db-mode-switch;
			};

			mhi_chan@101 {
				reg = <101>;
				label = "IP_HW0";
				mhi,num-elements = <512>;
				mhi,event-ring = <7>;
				mhi,chan-dir = <2>;
				mhi,data-type = <4>;
				mhi,doorbell-mode = <3>;
				mhi,ee = <0x4>;
			};
		};

		mhi_events {
			mhi_event@0 {
				mhi,num-elements = <32>;
				mhi,intmod = <1>;
				mhi,msi = <1>;
				mhi,priority = <1>;
				mhi,brstmode = <2>;
				mhi,data-type = <1>;
			};

			mhi_event@1 {
				mhi,num-elements = <256>;
				mhi,intmod = <1>;
				mhi,msi = <2>;
				mhi,priority = <1>;
				mhi,brstmode = <2>;
			};

			mhi_event@2 {
				mhi,num-elements = <256>;
				mhi,intmod = <1>;
				mhi,msi = <3>;
				mhi,priority = <1>;
				mhi,brstmode = <2>;
			};

			mhi_event@3 {
				mhi,num-elements = <256>;
				mhi,intmod = <1>;
				mhi,msi = <4>;
				mhi,priority = <1>;
				mhi,brstmode = <2>;
			};

			mhi_event@4 {
				mhi,num-elements = <1024>;
				mhi,intmod = <5>;
				mhi,msi = <5>;
				mhi,chan = <46>;
				mhi,priority = <1>;
				mhi,brstmode = <2>;
			};

			mhi_event@5 {
				mhi,num-elements = <1024>;
				mhi,intmod = <5>;
				mhi,msi = <6>;
				mhi,chan = <47>;
				mhi,priority = <1>;
				mhi,brstmode = <2>;
				mhi,client-manage;
			};

			mhi_event@6 {
				mhi,num-elements = <1024>;
				mhi,intmod = <5>;
				mhi,msi = <5>;
				mhi,chan = <100>;
				mhi,priority = <1>;
				mhi,brstmode = <3>;
				mhi,hw-ev;
			};

			mhi_event@7 {
				mhi,num-elements = <1024>;
				mhi,intmod = <5>;
				mhi,msi = <6>;
				mhi,chan = <101>;
				mhi,priority = <1>;
				mhi,brstmode = <3>;
				mhi,hw-ev;
				mhi,client-manage;
			};
		};

		mhi_devices {
			#address-cells = <1>;
			#size-cells = <0>;

			mhi_netdev_5: mhi_rmnet@0 {
				reg = <0x0>;
				mhi,chan = "IP_HW0";
				mhi,interface-name = "rmnet_mhi";
				mhi,mru = <0x4000>;
				mhi,disable-chain-skb;
			};

			mhi_netdev_7: mhi_rmnet@2 {
				reg = <0x2>;
				mhi,chan = "IP_SW0";
				mhi,interface-name = "mhi_swip";
				mhi,mru = <0x4000>;
				mhi,disable-chain-skb;
			};
		};
	};
};
