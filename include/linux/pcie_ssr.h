/* Copyright (c) 2020, The Linux Foundation. All rights reserved.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 and
* only version 2 as published by the Free Software Foundation.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*/

#ifndef __PCIE_SSR_H__
#define __PCIE_SSR_H__

#ifdef CONFIG_QTI_PCIE_SSR
void register_pcie_ssr(void *priv);
#else
void register_pcie_ssr(void *priv)
{}
#endif
int mhi_arch_pcie_ssr_ops_power_on(void *priv);
void mhi_arch_pcie_ssr_ops_power_off(void *priv);

#endif
